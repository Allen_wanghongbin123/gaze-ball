﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SightPlusMiddleware
{
    public class SightPlusSerializeBase : MonoBehaviour, ISerializationCallbackReceiver
    {

        [SerializeField]
        private List<string> variableNames = new List<string>();
        [SerializeField]
        private List<string> variableList = new List<string>();
        private Dictionary<string, string> variableDic = new Dictionary<string, string>();

        [SerializeField]
        private List<string> componentNames = new List<string>();
        [SerializeField]
        private List<Component> componentList = new List<Component>();
        private Dictionary<string, Component> componentDic = new Dictionary<string, Component>();

        public virtual void SaveFields()
        {
            variableDic.Clear();
            componentDic.Clear();
        }

        public virtual void ReadFields()
        {

        }

        #region Add and Get methods
        public void AddSerializeVariable(string name, object value)
        {
            try
            {
                variableDic.Add(name, value != null ? value.ToString() : null);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Add variable key: " + name + "\n" + e.ToString());
            }
        }

        public void AddSerializeComponent(string name, Component com)
        {
            try
            {
                componentDic.Add(name, com);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Add component key: " + name + "\n" + e.ToString());
            }
        }

        public string GetSerializeVariable(string name)
        {
            try
            {
                return variableDic[name];
            }
            catch (System.Exception e)
            {
#if UNITY_EDITOR
                Debug.LogWarning("Get variable key: " + name + " is null\nIs't an old bundle?\n" + e.ToString());
#endif
            }

            return null;
        }

        public Component GetSerializeComponent(string name)
        {
            try
            {
                return componentDic[name];
            }
            catch (System.Exception e)
            {
#if UNITY_EDITOR
                Debug.LogWarning("Get component key: " + name + " is null\nIs't an old bundle?\n" + e.ToString());
#endif
            }

            return null;
        }
        #endregion

        public void OnBeforeSerialize()
        {
            variableNames.Clear();
            variableList.Clear();
            foreach (var kvp in variableDic)
            {
                variableNames.Add(kvp.Key);
                variableList.Add(kvp.Value);
            }

            componentNames.Clear();
            componentList.Clear();
            foreach (var kvp in componentDic)
            {
                componentNames.Add(kvp.Key);
                componentList.Add(kvp.Value);
            }

            SaveFields();
        }

        public void OnAfterDeserialize()
        {
            variableDic = new Dictionary<string, string>();
            for (int i = 0; i != Mathf.Min(variableNames.Count, variableList.Count); i++)
            {
                variableDic.Add(variableNames[i], variableList[i]);
            }

            componentDic = new Dictionary<string, Component>();
            for (int i = 0; i != Mathf.Min(componentNames.Count, componentList.Count); i++)
            {
                componentDic.Add(componentNames[i], componentList[i]);
            }

            ReadFields();
        }
    }
}