﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(OnAutoDestroy());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator OnAutoDestroy()
    {
        yield return new WaitForSeconds(8f);
        Destroy(gameObject);
    }
}
