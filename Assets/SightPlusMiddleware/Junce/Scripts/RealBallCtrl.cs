﻿using UnityEngine;
using System.Collections;

public class RealBallCtrl : MonoBehaviour {

    public AudioClip ballShort;
    public AudioClip ballLong;
    public AudioClip ballInBasket;
    public AudioClip basketShort;
    public AudioClip basketLong;

    public Transform ballAudio;

    private int m_WallEnterCount = 2;

    private float m_Distance;
    public GameObject m_BasketTrigger;

    // Use this for initialization
    void Start()
    {
        m_Distance = Vector3.Distance(transform.position, m_BasketTrigger.transform.position);
    }

    public Transform shadow;

    // Update is called once per frame
    void Update()
    {
        shadow.position = new Vector3(transform.position.x, 0f, transform.position.z);
        shadow.rotation = Quaternion.identity;
        float distance = Vector3.Distance(transform.position, shadow.position);
        if (distance < 5f)
        {
            float alpha = Mathf.Lerp(0.5f, 0f, distance / 5f);
            shadow.GetChild(0).renderer.material.color = new Color(1f, 1f, 1f, alpha);
        }
        else
        {
            shadow.GetChild(0).renderer.material.color = new Color(1f, 1f, 1f, 0f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Wall")
        {
            if (m_WallEnterCount <= 0)
            {
                Destroy(gameObject);
            }
            m_WallEnterCount--;
            Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
            newBallAudio.gameObject.SetActive(true);
            newBallAudio.audio.clip = ballLong;
            newBallAudio.audio.Play();
        }
        else if (collision.collider.name == "Basketball(Clone)")
        {
            Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
            newBallAudio.gameObject.SetActive(true);
            newBallAudio.audio.clip = ballShort;
            newBallAudio.audio.Play();
        }
        else if (collision.collider.name == "reduce01:pTorus1Shape")
        {
            Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
            newBallAudio.gameObject.SetActive(true);
            newBallAudio.audio.clip = basketLong;
            newBallAudio.audio.volume = 0.3f;
            newBallAudio.audio.Play();
        }
        else if (collision.collider.name == "BasketStand")
        {
            Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
            newBallAudio.gameObject.SetActive(true);
            newBallAudio.audio.clip = ballShort;
            newBallAudio.audio.Play();
        }
        else
        {
            Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
            newBallAudio.gameObject.SetActive(true);
            newBallAudio.audio.clip = ballShort;
            newBallAudio.audio.Play();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "BasketTrigger")
        {
            if (transform.position.y > m_BasketTrigger.transform.position.y)
            {
                FindObjectOfType<BasketBallCtrl>().BallIn(m_Distance, gameObject);

                Transform newBallAudio = Instantiate(ballAudio, ballAudio.position, Quaternion.identity) as Transform;
                newBallAudio.gameObject.SetActive(true);
                newBallAudio.audio.clip = ballInBasket;
                newBallAudio.audio.Play();
            }
        }
    }
}
