﻿using UnityEngine;
using System.Collections;

public class RealNumberUI : MonoBehaviour
{

    public int NumberValue { get; set; }
    private int m_RecValue = 0;

    public Transform[] number100;
    public Transform[] number10;
    public Transform[] number1;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (m_RecValue != NumberValue)
        {
            m_RecValue = NumberValue;
            ChangeValue();
        }
	}

    void ChangeValue()
    {
        int temp = NumberValue % 1000 / 100;
        for (int i = 0; i < number100.Length; i++)
        {
            if (i == temp)
            {
                number100[i].gameObject.SetActive(true);
            }
            else
            {
                number100[i].gameObject.SetActive(false);
            }
        }
        temp = NumberValue % 100 / 10;
        for (int i = 0; i < number10.Length; i++)
        {
            if (i == temp)
            {
                number10[i].gameObject.SetActive(true);
            }
            else
            {
                number10[i].gameObject.SetActive(false);
            }
        }
        temp = NumberValue % 10;
        for (int i = 0; i < number1.Length; i++)
        {
            if (i == temp)
            {
                number1[i].gameObject.SetActive(true);
            }
            else
            {
                number1[i].gameObject.SetActive(false);
            }
        }
    }
}
