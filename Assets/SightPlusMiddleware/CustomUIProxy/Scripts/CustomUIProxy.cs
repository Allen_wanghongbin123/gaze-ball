﻿using UnityEngine;
using System.Collections;
using SightPlusInterface;

namespace SightPlusMiddleware
{
    public class CustomUIProxy : SightPlusSerializeBase
    {

        private AbstractCustomUIBehaviour uiBehaviour;

        public override void SaveFields()
        {
            base.SaveFields();
        }

        public override void ReadFields()
        {
            base.ReadFields();
        }

        void HideUI()
        {
            if (uiBehaviour != null)
            {
                uiBehaviour.HideUI();
            }
        }

        void ShowUI()
        {
            if (uiBehaviour != null)
            {
                uiBehaviour.ShowUI();
            }
        }

        void Awake()
        {
#if UNITY_EDITOR
            Object prefab = Resources.Load("DummyCustomUI");
#else
            Object prefab = Resources.Load("CustomUIController");
#endif
            GameObject go = Instantiate(prefab) as GameObject;
            go.transform.SetParent(transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;

            uiBehaviour = go.GetComponent<AbstractCustomUIBehaviour>();
        }
    } 
}
