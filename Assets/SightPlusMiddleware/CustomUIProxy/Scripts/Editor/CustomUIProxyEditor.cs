﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SightPlusMiddleware.CustomUIProxy))]
public class CustomUIProxyEditor : Editor {

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("");

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        EditorGUILayout.SelectableLabel("use ShowUI() and HideUI()");
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }
}
