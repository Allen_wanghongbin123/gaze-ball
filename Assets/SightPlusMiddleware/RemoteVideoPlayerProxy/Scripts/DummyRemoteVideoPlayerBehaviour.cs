﻿using UnityEngine;
using System.Collections;
using SightPlusInterface;

namespace SightPlusMiddleware
{
    public class DummyRemoteVideoPlayerBehaviour : AbstractRemoteVideoPlayerBehaviour
    {
        private bool isPlaying = false;
        public override void SetVideoWidget(string id, GameObject videoParent, System.Action endCallback)
        {
            
            return;
        }

        public override void SetAutoPlay(bool auto)
        {
            return;
        }

        public override void SetLoopCount(int loop)
        {
            return;
        }

        public override bool SetVolume(float value)
        {
            return true;
        }

        public override bool SetFullscreen(bool fullscreen)
        {
            return true;
        }

        public override int GetVideoWidth()
        {
            return 0;
        }

        public override int GetVideoHeight()
        {
            return 0;
        }

        public override float GetVideoLength()
        {
            return 0;
        }
        public override float GetCurrentPosition()
        {
            return 0;
        }

        public override bool IsPlaying()
        {
            return false;
        }

        public override bool IsEnd()
        {
            return false;
        }

        public override bool IsDownloaded()
        {
            return false;
        }

        public override bool Play()
        {
            isPlaying = true;
            return isPlaying;
        }

        public override bool Pause()
        {
            isPlaying = false;
            return true;
        }

        public override bool Stop()
        {
            isPlaying = false;
            return true;
        }

        public override bool SeekTo(float position)
        {
            return true;
        }
    }
}
