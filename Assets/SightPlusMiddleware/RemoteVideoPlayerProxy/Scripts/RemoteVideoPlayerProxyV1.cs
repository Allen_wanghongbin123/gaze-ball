﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SightPlusInterface;
using System.IO;

namespace SightPlusMiddleware
{
    public class RemoteVideoPlayerProxyV1 : SightPlusSerializeBase
    {
        [System.NonSerialized]
        public string VideoWidgetId = "";
        [System.NonSerialized]
        public string VideoWidgetUuid = "";

        [System.NonSerialized]
        public string VideoWidgetName = "";
        [System.NonSerialized]
        public int Priority = 10000;
        [System.NonSerialized]
        public string VideoResourceGroupId = "";
        [System.NonSerialized]
        public string VideoResourceGroupUuid = "";
        [System.NonSerialized]
        public string VideoPath = "";
        [System.NonSerialized]
        public bool AutoScale = true;
        [System.NonSerialized]
        public bool AutoPlay = true;
        [System.NonSerialized]
        public int LoopCount = 1;
        [System.NonSerialized]
        public int TransparentType = 0;
        [System.NonSerialized]
        public Color Chroma;
        [System.NonSerialized]
        public float Thresh1;
        [System.NonSerialized]
        public float Thresh2;
        [System.NonSerialized]
        public bool FullScreen = false;
        [System.NonSerialized]
        public int FullScreenDelay = 120;

        [System.NonSerialized]
        public Transform VideoParent;
        [System.NonSerialized]
        public Component CallbackCom;

        public int UploadProgress { get; private set; }
        public bool IsUpdating { get; private set; }
        public bool IsUploading { get; private set; }
        public bool IsQuerying { get; private set; }

        private AbstractRemoteVideoPlayerBehaviour RemoteVideo;

        public int DownloadProgress
        {
            get
            {
                if (RemoteVideo != null)
                {
                    return RemoteVideo.GetDownloadProgress();
                }
                return 0;
            }
        }

        public bool IsDownloaded
        {
            get
            {
                if (RemoteVideo != null)
                {
                    return RemoteVideo.IsDownloaded();
                }
                return false;
            }
        }

        public bool IsPlaying
        {
            get
            {
                if (RemoteVideo != null)
                {
                    return RemoteVideo.IsPlaying();
                }
                return false;
            }
        }

        public bool IsEnd
        {
            get
            {
                if (RemoteVideo != null)
                {
                    return RemoteVideo.IsEnd();
                }
                return false;
            }
        }

        // provide the most frequently used function.
        public void Play()
        {
            if (RemoteVideo != null)
            {
                RemoteVideo.Play();
            }
        }
        public void Restart()
        {
            if (RemoteVideo != null)
            {
                RemoteVideo.Restart();
            }
        }

        public void Pause()
        {
            if (RemoteVideo != null)
            {
                RemoteVideo.Pause();
            }
        }

        public void Stop()
        {
            if (RemoteVideo != null)
            {
                RemoteVideo.SeekTo(0);
                RemoteVideo.Stop();
            }
        }

        void OnVideoEnd()
        {
            if (CallbackCom != null)
            {
                // try use PlayMakerFSM SendEvent method send a RemoteProxyVideoEnd event
                CallbackCom.SendMessage("SendEvent", "RemoteProxyVideoEnd", SendMessageOptions.DontRequireReceiver);
            }
        }

        // Use this for initialization
        void Awake()
        {
            if (VideoWidgetId == "")
            {
                Debug.LogError("Widget Id is empty");
                return;
            }
#if UNITY_EDITOR
            Object prefab = Resources.Load("DummyRemoteVideo");
#else
            Object prefab = Resources.Load("SelfDownloadVideoWidget");
#endif
            GameObject go = Instantiate(prefab) as GameObject;
            RemoteVideo = go.GetComponent<AbstractRemoteVideoPlayerBehaviour>();

            RemoteVideo.gameObject.transform.parent = gameObject.transform;
            RemoteVideo.gameObject.transform.localScale = Vector3.one;
            RemoteVideo.gameObject.transform.localPosition = Vector3.zero;
            RemoteVideo.gameObject.transform.localRotation = Quaternion.identity;

            RemoteVideo.SetVideoWidget(VideoWidgetId, VideoParent.gameObject, OnVideoEnd);

            RemoteVideo.gameObject.SetActive(true);
        }

        public override void SaveFields()
        {
            base.SaveFields();

            AddSerializeVariable("VideoWidgetId", VideoWidgetId);
            AddSerializeVariable("VideoWidgetUuid", VideoWidgetUuid);
            AddSerializeVariable("VideoWidgetName", VideoWidgetName);
            AddSerializeVariable("Priority", Priority);
            AddSerializeVariable("VideoResourceGroupId", VideoResourceGroupId);
            AddSerializeVariable("VideoResourceGroupUuid", VideoResourceGroupUuid);
            AddSerializeVariable("VideoPath", VideoPath);
            AddSerializeVariable("AutoScale", AutoScale);
            AddSerializeVariable("AutoPlay", AutoPlay);
            AddSerializeVariable("LoopCount", LoopCount);
            AddSerializeVariable("TransparentType", TransparentType);
            AddSerializeVariable("FullScreen", FullScreen);
            AddSerializeVariable("FullScreenDelay", FullScreenDelay);

            AddSerializeComponent("VideoParent", VideoParent);
            AddSerializeComponent("CallbackCom", CallbackCom);
        }

        public override void ReadFields()
        {
            base.ReadFields();

            VideoWidgetId = GetSerializeVariable("VideoWidgetId");
            VideoWidgetUuid = GetSerializeVariable("VideoWidgetUuid");
            VideoWidgetName = GetSerializeVariable("VideoWidgetName");
            int.TryParse(GetSerializeVariable("Priority"), out Priority);
            VideoResourceGroupId = GetSerializeVariable("VideoResourceGroupId");
            VideoResourceGroupUuid = GetSerializeVariable("VideoResourceGroupUuid");
            VideoPath = GetSerializeVariable("VideoPath");
            AutoScale = GetSerializeVariable("AutoScale") == "True" ? true : false;
            AutoPlay = GetSerializeVariable("AutoPlay") == "True" ? true : false;
            int.TryParse(GetSerializeVariable("LoopCount"), out Priority);
            int.TryParse(GetSerializeVariable("TransparentType"), out TransparentType);
            FullScreen = GetSerializeVariable("FullScreen") == "True" ? true : false;
            int.TryParse(GetSerializeVariable("FullScreenDelay"), out FullScreenDelay);

            VideoParent = GetSerializeComponent("VideoParent") as Transform;
            CallbackCom = GetSerializeComponent("CallbackCom");
        }

        public static void StartUpload(RemoteVideoPlayerProxyV1 video, bool createNewOne)
        {
            video.UploadProgress = 0;
            video.IsUploading = true;
            video.StartCoroutine(video.OnUploadNewVideo(createNewOne));
        }

        public static void QueryWidgetInfo(RemoteVideoPlayerProxyV1 video)
        {
            video.IsQuerying = true;
            video.StartCoroutine(video.OnQueryWidgetInfo());
        }

        public static void UpdateWidget(RemoteVideoPlayerProxyV1 video)
        {
            video.IsUpdating = true;
            video.StartCoroutine(video.OnUpdateWidget());
        }

        IEnumerator OnUploadNewVideo(bool createNewOne)
        {
            if (File.Exists(VideoPath))
            {
                FileStream fs = new FileStream(VideoPath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] data = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();

                WWWForm form = new WWWForm();
                form.AddField("userId", "30");
                form.AddField("character", "resolution");
                form.AddField("characterSubType", "lowRes");
                form.AddField("res", "");
                form.AddBinaryData("res", data, System.IO.Path.GetFileName(VideoPath), null);

                using (WWW www = new WWW("http://res.sightp.com/index.php/ResourceUploadApi/UploadVideo", form))
                {
                    while (!www.isDone)
                    {
                        UploadProgress = (int)(www.uploadProgress * 100f);
                        yield return new WaitForEndOfFrame();
                    }

                    if (www.error == null)
                    {
                        Hashtable args = MiniJSON.jsonDecode(www.text) as Hashtable;
                        if (args["errorCode"].ToString() == "0")
                        {
                            Hashtable result = args["result"] as Hashtable;
                            VideoResourceGroupId = result["resourceGroupId"].ToString();
                            VideoResourceGroupUuid = result["resourceGroupUuid"].ToString();

                            if (createNewOne)
                            {
                                yield return StartCoroutine(OnCreateWidget());
                                yield return StartCoroutine(OnQueryWidgetInfo());
                            }
                            else
                            {
                                yield return StartCoroutine(OnUpdateWidget());
                            }
                        }
                        else
                        {
                            Debug.LogError(args["errorCode"].ToString());
                        }
                    }
                    else
                    {
                        Debug.LogError(www.error);
                    }
                }
            }

            IsUploading = false;

            yield return 0;

        }

        IEnumerator OnCreateWidget()
        {
            WWWForm form = new WWWForm();
            form.AddField("userId", "30");
            form.AddField("resourceGroupId", VideoResourceGroupId);
            form.AddField("resourceGroupUuid", VideoResourceGroupUuid);

            using (WWW www = new WWW("http://console.sightp.com/service/index.php/SightPlusToolApi/CreateVideoWidget", form))
            {
                yield return www;

                if (www.error == null)
                {
                    Hashtable args = MiniJSON.jsonDecode(www.text) as Hashtable;
                    if (args["errorCode"].ToString() == "0")
                    {
                        Hashtable result = args["result"] as Hashtable;
                        VideoWidgetId = result["id"].ToString();
                        VideoWidgetUuid = result["uuid"].ToString();

                    }
                    else
                    {
                        Debug.LogError(args["errorCode"].ToString());
                    }
                }
                else
                {
                    Debug.LogError(www.error);
                }
            }
            yield return 0;
        }

        IEnumerator OnUpdateWidget()
        {
            WWWForm form = new WWWForm();
            form.AddField("userId", "30");
            form.AddField("id", VideoWidgetId);
            form.AddField("name", VideoWidgetName);
            form.AddField("priority", Priority);
            form.AddField("positionX", transform.localPosition.x.ToString());
            form.AddField("positionY", transform.localPosition.y.ToString());
            form.AddField("positionZ", transform.localPosition.z.ToString());
            form.AddField("scaleX", transform.localScale.x.ToString());
            form.AddField("scaleY", transform.localScale.y.ToString());
            form.AddField("scaleZ", transform.localScale.z.ToString());
            form.AddField("rotationX", transform.localRotation.eulerAngles.x.ToString());
            form.AddField("rotationY", transform.localRotation.eulerAngles.y.ToString());
            form.AddField("rotationZ", transform.localRotation.eulerAngles.z.ToString());
            form.AddField("autoScale", AutoScale ? "1" : "0");
            form.AddField("autoPlay", AutoPlay ? "1" : "0");
            form.AddField("loopCount", LoopCount.ToString());
            form.AddField("fullScreenEnable", FullScreen ? "1" : "0");
            form.AddField("fullScreenDelay", FullScreenDelay.ToString());
            form.AddField("resourceGroupId", VideoResourceGroupId);
            form.AddField("resourceGroupUuid", VideoResourceGroupUuid);

            using (WWW www = new WWW("http://console.sightp.com/service/index.php/SightPlusToolApi/UpdateVideoWidget", form))
            {
                yield return www;

                if (www.error == null)
                {
                    Hashtable args = MiniJSON.jsonDecode(www.text) as Hashtable;
                    if (args["errorCode"].ToString() == "0")
                    {
                        if (args["result"].ToString() != "Success")
                        {
                            Debug.LogError("Update video widget fail.");
                        }
                    }
                    else
                    {
                        Debug.LogError(args["errorCode"].ToString());
                    }
                }
                else
                {
                    Debug.LogError(www.error);
                }
            }
            IsUpdating = false;

            yield return 0;
        }

        IEnumerator OnQueryWidgetInfo()
        {
            WWWForm form = new WWWForm();
            form.AddField("id", VideoWidgetId);

            using (WWW www = new WWW("http://console.sightp.com/service/index.php/SightPlusToolApi/queryVideoWidget", form))
            {
                yield return www;

                if (www.error == null)
                {
                    Hashtable args = MiniJSON.jsonDecode(www.text) as Hashtable;
                    if (args["errorCode"].ToString() == "0")
                    {
                        Hashtable result = args["result"] as Hashtable;
                        Hashtable resourceGroup = result["resourceGroup"] as Hashtable;
                        VideoResourceGroupId = resourceGroup["id"].ToString();
                        VideoResourceGroupUuid = resourceGroup["uuid"].ToString();
                        VideoWidgetName = result["name"].ToString();
                        Priority = int.Parse(result["priority"].ToString());
                        AutoScale = result["autoScale"].ToString() == "1" ? true : false;
                        AutoPlay = result["autoPlay"].ToString() == "1" ? true : false;
                        LoopCount = int.Parse(result["loopCount"].ToString());
                        TransparentType = int.Parse((result["transparentInfo"] as Hashtable)["type"].ToString());
                        //Chroma
                        //Thresh1
                        //Thresh2
                        Hashtable fullScreenInfo = result["fullScreenInfo"] as Hashtable;
                        FullScreen = fullScreenInfo["enable"].ToString() == "1" ? true : false;
                        FullScreenDelay = int.Parse(fullScreenInfo["delay"].ToString());

                        //ArrayList position = result["position"] as ArrayList;
                        //transform.localPosition = new Vector3(float.Parse(position[0].ToString()), float.Parse(position[1].ToString()), float.Parse(position[2].ToString()));
                        //ArrayList scale = result["scale"] as ArrayList;
                        //transform.localScale = new Vector3(float.Parse(scale[0].ToString()), float.Parse(scale[1].ToString()), float.Parse(scale[2].ToString()));
                        //ArrayList rotation = result["rotation"] as ArrayList;
                        //transform.localRotation = Quaternion.Euler(float.Parse(rotation[0].ToString()), float.Parse(rotation[1].ToString()), float.Parse(rotation[2].ToString()));
                    }
                    else
                    {
                        Debug.LogError(args["errorCode"].ToString());
                    }
                }
                else
                {
                    Debug.LogError(www.error);
                }
            }
            IsQuerying = false;

            yield return 0;
        }
    }
}
