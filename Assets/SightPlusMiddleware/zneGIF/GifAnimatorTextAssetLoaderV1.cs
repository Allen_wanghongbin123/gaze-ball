﻿using UnityEngine;
using System.Collections;

namespace SightPlusMiddleware
{
    public class GifAnimatorTextAssetLoaderV1 : MonoBehaviour
    {
        public TextAsset gifAsset;

        private zneGifAnimation zneGif;

        // Use this for initialization
        void Start()
        {
            zneGif = GetComponent<zneGifAnimation>();
            if (zneGif == null)
            {
                Debug.LogError("Cannot find zneGifAnimation component");
            }
            else
            {
                try
                {
                    System.IO.Stream stream = new System.IO.MemoryStream(gifAsset.bytes);
                    zneGif.LoadGifClipByStream(stream);
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.ToString());
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnDestroy()
        {
            for (int i = 0; i < zneGif.Clip._frames.Length; i++)
            {
                zneGif.Clip._frames[i]._texture = null;
            }
            Resources.UnloadUnusedAssets();
        }
    }
}
