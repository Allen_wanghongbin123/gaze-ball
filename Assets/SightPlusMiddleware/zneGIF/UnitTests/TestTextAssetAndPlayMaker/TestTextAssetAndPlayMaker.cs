﻿using UnityEngine;
using System.Collections;

public class TestTextAssetAndPlayMaker : MonoBehaviour {
	public zneGifAnimation gif;
	private string platformPrefix;

	public string filename;

	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		platformPrefix = @"file://";
		#elif UNITY_IPHONE
		platformPrefix = @"file://";
		#elif UNITY_ANDROID
		platformPrefix = @"";
		#endif

		StartCoroutine(Load());
	}
	
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Load() {
		Caching.CleanCache();
	#if UNITY_EDITOR
		using (WWW model = WWW.LoadFromCacheOrDownload(platformPrefix + Application.streamingAssetsPath + @"/AssetBundles/" + filename + @"/" + filename + ".assetbundle", 1)) {
	#elif UNITY_ANDROID
			using (WWW model = WWW.LoadFromCacheOrDownload(platformPrefix + Application.streamingAssetsPath + @"/AssetBundles/" + filename + @"/" + filename + "ForAndroid.assetbundle", 1)) {
	#elif UNITY_IPHONE
				using (WWW model = WWW.LoadFromCacheOrDownload(platformPrefix + Application.streamingAssetsPath + @"/AssetBundles/" + filename + @"/" + filename + "ForIOS.assetbundle", 1)) {
	#endif
					yield return model;
					if (model.error == null) {
						Debug.Log("AssetBundle loaded");
						AssetBundle bundle = model.assetBundle;
						//TextAsset textAsset = bundle.Load("root") as TextAsset;
						GameObject prefab = bundle.Load("root") as GameObject;
						GameObject go = Instantiate(prefab) as GameObject;
                        go.name = "root";
						//Debug.Log(textAsset.bytes.Length);

						//System.IO.Stream stream = new System.IO.MemoryStream(go.GetComponent<TestGifBehaviourV1>().gifData.bytes);
						//gif.LoadGifClipByStream(stream);
					}
					else {
						Debug.Log(model.error.ToString());
					}
		}
	}
}
