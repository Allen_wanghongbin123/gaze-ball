﻿using UnityEngine;
using System.Collections;

namespace SightPlusMiddleware
{
    public class GifAnimatorMessageAdaptorV1 : MonoBehaviour
    {
        public PlayMakerFSM fsm;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnGIF_SetFrame(object val)
        {
            zneGIF.Frame frame = val as zneGIF.Frame;

            if (fsm != null)
            {
                HutongGames.PlayMaker.FsmVariables fsmVairiables = fsm.FsmVariables;
                HutongGames.PlayMaker.FsmInt curGifFrameIdx = fsmVairiables.GetFsmInt("curGifFrameIdx");
                curGifFrameIdx.Value = frame._index;

                fsm.SendEvent("NewGifFrame");

                Debug.Log("Got new gif frame " + frame._index);
            }
        }
    }
}
