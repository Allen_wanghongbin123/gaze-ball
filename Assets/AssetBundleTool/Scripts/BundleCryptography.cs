﻿using UnityEngine;
using System.Collections;

namespace EasyARBundleTool
{
    /// <summary>
    /// Bundle的版本号
    /// 一个Bundle版本对应一个个BundleGeneration，也即用来打包的unity3d的版本
    /// </summary>
    public enum BundleVersion
    {
        Ver1_0_0 = 0, // DES Encryption + Unity4.3.4(Bundle Gen_0)
        Ver2_0_0 = 1, // Naive Encryption + Unity4.3.4(Bundle Gen_0)
        Ver2_1_0 = 2,  // Naive Encryption + Unity4.6.1(Bundle Gen_1)
        Ver2_1_1 = 3,  // Naive Encryption + Unity4.6.3(Bundle Gen_1)
    }

    /// <summary>
    /// Bundle的Generation
    /// 一个Generation可能存在于不同的BundleVersion中。因为不同的BundleVersion可能有不同的打包方式，比如不同的加密方式。
    /// </summary>
    public enum BundleGeneration
    {
        Gen_0 = 0, // unity4.3.1
        Gen_1 // unity4.6.1 | unity4.6.3
    }

    public class BundleCryptography
    {
        private static byte[] Pack_Ver1_0_0(byte[] data)
        {
            return AddBundleHeader(DataCryptography.TripleDESEncryptData(data, BundleCryptoConfig.Triple_DES_Crypto_Key_Ver1_0_0, BundleCryptoConfig.Triple_DES_Crypto_IV_Ver1_0_0), (uint)BundleVersion.Ver1_0_0, (uint)BundleGeneration.Gen_0);
        }

        private static byte[] Unpack_Ver1_0_0(byte[] encryptedData)
        {
            return DataCryptography.TripleDESDecryptData(encryptedData, BundleCryptoConfig.Triple_DES_Crypto_Key_Ver1_0_0, BundleCryptoConfig.Triple_DES_Crypto_IV_Ver1_0_0);
        }

        private static byte[] Pack_Ver2_0_0(byte[] data)
        {
            return AddBundleHeader(DataCryptography.NaiveBitEncryptData(data, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0), (uint)BundleVersion.Ver2_0_0, (uint)BundleGeneration.Gen_0);
        }

        private static byte[] Unpack_Ver2_0_0(byte[] encryptedData)
        {
            return DataCryptography.NaiveBitDecryptData(encryptedData, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0);
        }

        private static byte[] Pack_Ver2_1_0(byte[] data)
        {
            return AddBundleHeader(DataCryptography.NaiveBitEncryptData(data, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0), (uint)BundleVersion.Ver2_1_0, (uint)BundleGeneration.Gen_1);
        }

        private static byte[] Unpack_Ver2_1_0(byte[] encryptedData)
        {
            return DataCryptography.NaiveBitDecryptData(encryptedData, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0);
        }

        private static byte[] Pack_Ver2_1_1(byte[] data)
        {
            return AddBundleHeader(DataCryptography.NaiveBitEncryptData(data, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0), (uint)BundleVersion.Ver2_1_0, (uint)BundleGeneration.Gen_1);
        }

        private static byte[] Unpack_Ver2_1_1(byte[] encryptedData)
        {
            return DataCryptography.NaiveBitDecryptData(encryptedData, BundleCryptoConfig.Naive_Bit_Crypto_Key_Ver2_0_0);
        }

        /// <summary>
        /// Encrypt the bundle data
        /// </summary>
        /// <param name="data">Source data</param>
        /// <param name="version">Encrypt version</param>
        /// <returns></returns>
        public static byte[] PackAssetBundle(byte[] data, BundleVersion version)
        {
            byte[] bundleData = null;

            //Add pack handler here
            switch (version)
            {
                case BundleVersion.Ver1_0_0:
                    bundleData = Pack_Ver1_0_0(data);
                    break;
                case BundleVersion.Ver2_0_0:
                    bundleData = Pack_Ver2_0_0(data);
                    break;
                case BundleVersion.Ver2_1_0:
                    bundleData = Pack_Ver2_1_0(data);
                    break;
                case BundleVersion.Ver2_1_1:
                    bundleData = Pack_Ver2_1_1(data);
                    break;
                default:
                    break;
            }

            return bundleData;
        }

        /// <summary>
        /// Decrypt the easyar bundle data
        /// </summary>
        /// <param name="bundleData">Source data</param>
        /// <returns></returns>
        public static byte[] UnpackAssetBundle(byte[] bundleData)
        {
            byte[] bundleHeader = new byte[4];
            for (int index = 0; index < 4; index++)
            {
                bundleHeader[index] = bundleData[index];
            }

            byte[] encryptedData = new byte[bundleData.Length - 128];
            System.Buffer.BlockCopy(bundleData, 128, encryptedData, 0, encryptedData.Length);

            byte[] data = null;
            uint version = BytesToUint(bundleHeader);

            //Add unpack handler here
            switch (version)
            {
                case (uint)BundleVersion.Ver1_0_0:
                    data = Unpack_Ver1_0_0(encryptedData);
                    break;
                case (uint)BundleVersion.Ver2_0_0:
                    data = Unpack_Ver2_0_0(encryptedData);
                    break;
                case (uint)BundleVersion.Ver2_1_0:
                    data = Unpack_Ver2_1_0(encryptedData);
                    break;
                case (uint)BundleVersion.Ver2_1_1:
                    data = Unpack_Ver2_1_1(encryptedData);
                    break;
                default:
                    break;
            }

            return data;
        }

        /// <summary>
        /// Add crypto info to the bundle
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="version">Version</param>
        /// <returns></returns>
        private static byte[] AddBundleHeader(byte[] data, uint bundleVersion, uint bundleGeneration)
        {
            byte[] bundleData = new byte[data.Length + 128];

            UintToBytes(bundleVersion).CopyTo(bundleData, 0);
            UintToBytes(bundleGeneration).CopyTo(bundleData, sizeof(uint));

            data.CopyTo(bundleData, 128);

            return bundleData;
        }

        private static byte[] UintToBytes(uint i)
        {
            byte[] bytes = new byte[4];
            bytes[0] = (byte)(0xff & i);
            bytes[1] = (byte)((0xff00 & i) >> 8);
            bytes[2] = (byte)((0xff0000 & i) >> 16);
            bytes[3] = (byte)((0xff000000 & i) >> 24);

            return bytes;
        }

        private static uint BytesToUint(byte[] bytes)
        {
            uint addr = (uint)bytes[0] & 0xff;
            addr |= (uint)((bytes[1] << 8) & 0xff00);
            addr |= (uint)((bytes[2] << 16) & 0xff0000);
            addr |= (uint)((bytes[3] << 24) & 0xff000000);

            return addr;
        }
    }
}
