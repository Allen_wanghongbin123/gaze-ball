﻿using UnityEngine;
using System.Collections;
using System.Threading;

namespace EasyARBundleTool
{
    public class AsyncLoadEasyARBundle : MonoBehaviour
    {

        public delegate void LoadCallback(AssetBundle assetBundle);

        private static AsyncLoadEasyARBundle mInstance = null;

        public static AsyncLoadEasyARBundle GetInstance()
        {
            if (mInstance != null)
            {
                return mInstance;
            }
            else
            {
                mInstance = FindObjectOfType<AsyncLoadEasyARBundle>();
                if (mInstance != null)
                {
                    return mInstance;
                }
                else
                {
                    GameObject go = new GameObject();
                    go.name = "AsyncLoad";
                    return go.AddComponent<AsyncLoadEasyARBundle>();
                }
            }
        }

        /// <summary>
        /// Load easyar bundle
        /// </summary>
        /// <param name="url">Bundle path</param>
        /// <param name="callback">Invoke when loading complete without error</param>
        public static void LoadAsset(string url, LoadCallback callback)
        {
            GetInstance().StartLoadAsset(url, callback);
        }

        public void StartLoadAsset(string url, LoadCallback callback)
        {
            StartCoroutine(OnLoadAsset(url, callback));
        }


        public class DecryptionThreadArgs
        {
            public byte[] srcBytes;
            public byte[] dstBytes;
        }

        void UnpackWorker(object args)
        {
            DecryptionThreadArgs _args = args as DecryptionThreadArgs;
            _args.dstBytes = BundleCryptography.UnpackAssetBundle(_args.srcBytes);

        }

        IEnumerator UnpackDataAsync(byte[] rawBytes, LoadCallback callback)
        {
            DecryptionThreadArgs args = new DecryptionThreadArgs();
            args.srcBytes = rawBytes;

            Thread thread = new Thread(this.UnpackWorker);

            thread.Start(args);

            while (thread.IsAlive)
            {
                yield return 0;
            }

            thread.Join();

            AssetBundleCreateRequest request = AssetBundle.CreateFromMemory(args.dstBytes);
            yield return request;
            callback(request.assetBundle);
            request.assetBundle.Unload(false);
        }

        private IEnumerator OnLoadAsset(string url, LoadCallback callback)
        {
            using (WWW www = new WWW(url))
            {
                yield return www;
                if (www.error == null)
                {
                    if (System.IO.Path.GetExtension(url).Equals(".easyarbundle"))
                    {
                        yield return StartCoroutine(UnpackDataAsync(www.bytes, callback));

                    }
                    else if (System.IO.Path.GetExtension(url).Equals(".assetbundle"))
                    {
                        callback(www.assetBundle);
                        www.assetBundle.Unload(false);
                    }
                    else
                    {
                        Debug.LogError("Wrong bundle!");
                    }
                }
            }
        }
    }
}
