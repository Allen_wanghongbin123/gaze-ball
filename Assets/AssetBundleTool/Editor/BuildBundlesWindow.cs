﻿using UnityEngine;
using UnityEditor;

namespace EasyARBundleTool
{
    public class BuildWindow : EditorWindow
    {
        private int ieId = 0;
        private BundleVersion bundleVersion = BundleVersion.Ver2_1_1; // TODO:: should use bundle version instead
        private BuildTarget buildTarget = BuildTarget.StandaloneWindows;

        private bool m_ShowCreateBtns = false;
        private bool m_ShowUpdateBtns = false;

        [MenuItem("EasyARBundle Tools/Build Bundle")]
        static void Init()
        {
            BuildWindow window = (BuildWindow)EditorWindow.GetWindow(typeof(BuildWindow));
            window.minSize = new Vector2(400f, 260f);
        }

        void SaveLastSetting()
        {
            PlayerPrefs.SetInt("EasyARBuildBundle_ieID", ieId);
            PlayerPrefs.SetInt("EasyARBuildBundle_BundleVersion", (int)bundleVersion);
            PlayerPrefs.SetInt("EasyARBuildBundle_BuildTarget", (int)buildTarget);

            Debug.Log("Save build setting completed.");
        }

        void LoadLastSetting()
        {
            ieId = PlayerPrefs.GetInt("EasyARBuildBundle_ieID", 0);
            bundleVersion = (BundleVersion)PlayerPrefs.GetInt("EasyARBuildBundle_BundleVersion", (int)BundleVersion.Ver2_1_1);
            buildTarget = (BuildTarget)PlayerPrefs.GetInt("EasyARBuildBundle_BuildTarget", (int)BuildTarget.StandaloneWindows);

            Debug.Log("Load build setting completed.");
        }

        void OnGUI()
        {
            //////////////////////////////////////////////////////////////////////////
            // Build setting
            GUILayout.Label("Build Setting " + BundleVersion.Ver2_1_1, EditorStyles.boldLabel);
            ieId = EditorGUILayout.IntField("ieID", ieId);

            //////////////////////////////////////////////////////////////////////////
            // Function buttons
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Create Bundle"))
            {
                m_ShowCreateBtns = true;
                m_ShowUpdateBtns = false;
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Update Bundle"))
            {
                m_ShowCreateBtns = false;
                m_ShowUpdateBtns = true;
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Use Last Setting"))
            {
                m_ShowCreateBtns = false;
                m_ShowUpdateBtns = false;

                // Load last update setting
                LoadLastSetting();
            }
            EditorGUILayout.EndHorizontal();

            //////////////////////////////////////////////////////////////////////////
            // Choose create platform
            if (m_ShowCreateBtns)
            {
                EditorGUILayout.LabelField("Create bundle on :");
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("All Platform"))
                {
                    m_ShowCreateBtns = false;
                    BuildBundles.CreateAssetBundlesAllPlatform();
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("Standalone"))
                {
                    m_ShowCreateBtns = false;
                    BuildBundles.CreateAssetBundles_Standalone();
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("IOS"))
                {
                    m_ShowCreateBtns = false;
                    BuildBundles.CreateAssetBundles_IOS();
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("Android"))
                {
                    m_ShowCreateBtns = false;
                    BuildBundles.CreateAssetBundles_Android();
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("WebPlayer"))
                {
                    m_ShowCreateBtns = false;
                    BuildBundles.CreateAssetBundles_WebPlayer();
                }
                EditorGUILayout.EndHorizontal();
            }

            //////////////////////////////////////////////////////////////////////////
            // Choose update platform
            if (m_ShowUpdateBtns)
            {
                BuildSettingArgs args = new BuildSettingArgs();
                args.id = 0;
                args.ieId = ieId;
                args.bundleVersion = bundleVersion;

                EditorGUILayout.LabelField("Update bundle on :");

                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("Standalone"))
                {
                    m_ShowUpdateBtns = false;

                    args.buildTarget = BuildTarget.StandaloneWindows;
                    SaveLastSetting();

                    string[] bundlesPath = BuildBundles.CreateAssetBundles_Standalone();

                    if (bundlesPath.Length == 0)
                    {
                        EditorUtility.DisplayDialog("错误", "没有生成bundle。", "确定");
                    }
                    else
                    {
                        UploadBundles.Upload(args, bundlesPath[0]);

                        if (bundlesPath.Length > 1)
                        {
                            EditorUtility.DisplayDialog("警告", "选中生成了多个Bundle，默认只上传第一个。", "确定");
                        }
                    }
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("IOS"))
                {
                    m_ShowUpdateBtns = false;

                    args.buildTarget = BuildTarget.iPhone;
                    SaveLastSetting();

                    string[] bundlesPath = BuildBundles.CreateAssetBundles_IOS();

                    if (bundlesPath.Length == 0)
                    {
                        EditorUtility.DisplayDialog("错误", "没有生成bundle。", "确定");
                    }
                    else
                    {
                        UploadBundles.Upload(args, bundlesPath[0]);

                        if(bundlesPath.Length > 1)
                        {
                            EditorUtility.DisplayDialog("警告", "选中生成了多个Bundle，默认只上传第一个。", "确定");
                        }
                    }
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("Android"))
                {
                    m_ShowUpdateBtns = false;

                    args.buildTarget = BuildTarget.Android;
                    SaveLastSetting();

                    string[] bundlesPath = BuildBundles.CreateAssetBundles_Android();

                    if (bundlesPath.Length == 0)
                    {
                        EditorUtility.DisplayDialog("错误", "没有生成bundle。", "确定");
                    }
                    else
                    {
                        UploadBundles.Upload(args, bundlesPath[0]);

                        if (bundlesPath.Length > 1)
                        {
                            EditorUtility.DisplayDialog("警告", "选中生成了多个Bundle，默认只上传第一个。", "确定");
                        }
                    }
                }
                EditorGUILayout.Space();
                if (GUILayout.Button("WebPlayer"))
                {
                    m_ShowUpdateBtns = false;

                    args.buildTarget = BuildTarget.WebPlayer;
                    SaveLastSetting();

                    string[] bundlesPath = BuildBundles.CreateAssetBundles_WebPlayer();

                    if (bundlesPath.Length == 0)
                    {
                        EditorUtility.DisplayDialog("错误", "没有生成bundle。", "确定");
                    }
                    else
                    {
                        UploadBundles.Upload(args, bundlesPath[0]);

                        if (bundlesPath.Length > 1)
                        {
                            EditorUtility.DisplayDialog("警告", "选中生成了多个Bundle，默认只上传第一个。", "确定");
                        }
                    }
                }

                EditorGUILayout.EndHorizontal();
            }
            if (BundleUploader.Instance.IsUploading)
            {
                EditorGUILayout.LabelField("Upload.." + (int)(BundleUploader.Instance.UploadProgress * 100f) + "%");
            }
        }

        void OnInspectorUpdate()
        {
            if (BundleUploader.Instance.IsUploading)
            {
                EditorUtility.SetDirty(BundleUploader.Instance);
                Repaint();
            }
        }

        public class BuildSettingArgs
        {
            public int id;
            public int ieId;
            public BundleVersion bundleVersion;
            public BuildTarget buildTarget;
        }
    }
}

